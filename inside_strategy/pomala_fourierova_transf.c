#include "pomala_fourierova_transf.h"

double pomala_fourierova_transf(double *pole, int n, int pocet_casti_int){

        double sum = 0;
        double *sin_pole;
        int i;
        sin_pole = malloc(sizeof(double) * pocet_casti_int);
        if(sin_pole == NULL){
        }

        for(i=0;i<pocet_casti_int;i++)
        {
                sin_pole[i] =sin(2*M_PI*(i+1)*n/pocet_casti_int;
        //      printf("sin_pole[%d] = %f\n",i, sin_pole[i]);
                sum += (pole[i] * sin_pole[i])/pocet_casti_int;
        }

        return sum;

}
