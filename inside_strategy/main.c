#include "funkcia_deliaca_interval.h"
#include "pomala_fourierova_transf.h"

int main()
{
        int n,i,pocet_casti_int;
        double *pole,pft;
	
	printf("Pocet period:");
        scanf("%d",&n);
	printf("Pocet casti intervalu:");
	scanf("%d",&pocet_casti_int);

        pole = funkcia_deliaca_interval(pocet_casti_int);
        for(i=0;i<n;i++)
        {
                pft = pomala_fourierova_transf(pole,(i+1),pocet_casti_int);
                printf("%d: %lf\n",i,pft);
        }

        return 0;
}
