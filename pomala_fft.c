#include <math.h>
#include <stdio.h>
#include <stdlib.h>


/* Funkcia y = x ~ f(x) = x */
double Nejaka_Funkcia_S_Funkcnymi_Hodnotami_Ktore_Chceme(double f)
{
    return f;
}

    /* Funkcia y = x^2 */
double Nejaka_INA_Funkcia_S_Funkcnymi_Hodnotami_Ktore_Chceme(double f)
{
    return f * f;
}

double obdlznikovy_signal_funkcia(double f)
{
	if (f <= 0.5)
		return 1;
	else 
		return 0;
}

double pilovity_signal_funkcia(double f)
{
	return f-floor(f);
}


    /* Rozdeli interval na 256 casti a priradim prvkom pola funkcne hodnoty vybranej funkcie */ 
double *funkcia_deliacia_interval(int pocet_casti_int){
	double *pole;
	int i;
	
	pole = malloc(sizeof(double) * pocet_casti_int);
	if(pole == NULL) {
	}
		    /* Priradovanie hodnot */
	for(i=0;i<pocet_casti_int;i++) {
		pole[i] =pilovity_signal_funkcia((double) (i+1)/pocet_casti_int);					
	//	printf("pole[%d] = %f\n",i, pole[i]);
	}
	
	return pole;
}

double pomala_fourierova_transf(double *pole, int n, int pocet_casti_int){
	double sum = 0;
	double *sin_pole;
	int i;
	sin_pole = malloc(sizeof(double) * pocet_casti_int);
	if(sin_pole == NULL){
	}
	
	for(i=0;i<pocet_casti_int;i++)
	{
		sin_pole[i] = sin(2*M_PI*(i+1)*n/pocet_casti_int);
	//	printf("sin_pole[%d] = %f\n",i, sin_pole[i]);
		sum += (pole[i] * sin_pole[i])/pocet_casti_int;  
	}
	
	return sum;
}


int main()
{
	int n,i,pocet_casti_int;
	double *pole,pft;
	printf("Pocet period:");
	scanf("%d",&n);
	printf("Pocet casti intervalu:");
	scanf("%d",&pocet_casti_int);
	
	pole = funkcia_deliacia_interval(pocet_casti_int);
	for(i=0;i<n;i++)
	{
		pft = pomala_fourierova_transf(pole,(1+i),pocet_casti_int);	
		printf("%d: %lf\n",i+1,pft);
	}
	
	return 0;	
}
