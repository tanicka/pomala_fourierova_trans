#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

double *mocniny_omega(int n, int pocet_casti_int){

	double *m_omega;
	int i;
	m_omega = malloc(sizeof(double) * pocet_casti_int);
	if(m_omega == NULL){
	}
	
	for(i=0;i<pocet_casti_int;i++)
	{
		m_omega[i] = sin(2*M_PI*(i+1)*n/pocet_casti_int);
	//	printf("sin_pole[%d] = %f\n",i, m_omega[i]);
	}
	
	return m_omega;
}

double pilovity_signal_funkcia(double f)
{
	return f-floor(f);
}

double *funkcia_deliacia_interval(int pocet_casti_int){
	double *pole;
	int i;
	
	pole = malloc(sizeof(double) * pocet_casti_int);
	if(pole == NULL) {
	}
		    /* Priradovanie hodnot */
	for(i=0;i<pocet_casti_int;i++) {
		pole[i] =pilovity_signal_funkcia((double) (i+1)/pocet_casti_int);					
	//	printf("pole[%d] = %f\n",i, pole[i]);
	}
	
	return pole;
}

double fft(double *m_omega,double *pole_hodnot_nejakej_vstupnej_funkcie,int pocet_casti_int, int n)
{
	double complex omega=I; //komplexne cislo
	int j,k;
//	int n=10; //velkost pole_hodnot_nejakej_vstupnej_funkcie;
	double *parne,*neparne,*koeficienty;
	parne=malloc(sizeof(double) * pocet_casti_int);
	neparne=malloc(sizeof(double) * pocet_casti_int);
	koeficienty=malloc(sizeof(double) * n);
	for(j=0;j<n;j++)
	{
		if(j%2==0) parne[j]=pole_hodnot_nejakej_vstupnej_funkcie[j];
		else neparne[j]=pole_hodnot_nejakej_vstupnej_funkcie[j];
	}
	
	for(j=0;j<n;j++)
	{
		for(k=0;k<n;k++)
		{
			koeficienty[j]+=(cpow(omega,m_omega[j]*m_omega[k]))*pole_hodnot_nejakej_vstupnej_funkcie[j];
		//	if((k%2)==0) koeficienty[j]+=(cpow(omega,2*m_omega[j]*m_omega[k]))*parne[j];
		//	else koeficienty[j]+=(cpow(omega,m_omega[j]))*(cpow(omega,2*m_omega[j]*m_omega[k]))*neparne[j];
			
		}		printf("%f\n",koeficienty[j]);
	}	
}

int main()
{
	double *m_omega,*pole_hodnot_nejakej_vstupnej_funkcie;
	m_omega=mocniny_omega(10,255);
	pole_hodnot_nejakej_vstupnej_funkcie=funkcia_deliacia_interval(255);
	fft(m_omega,pole_hodnot_nejakej_vstupnej_funkcie,255,10);
	
	return 0;
}
